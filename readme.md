# Angular
## Before You Start
Before you start learning *Angular* you should have some basic knowledge about the following topics

- [HTML](https://developer.mozilla.org/en-US/docs/Learn/HTML)
- [CSS](https://developer.mozilla.org/en-US/docs/Learn/CSS)
- [JavaScript](https://developer.mozilla.org/en-US/docs/Learn/JavaScript)
- [Typescript](https://www.typescriptlang.org/)

## What You Need
If you want to develop programs with *Angular*, then you need the following programs and tools

- [Node.js](https://nodejs.org/en)
- [npm](https://www.npmjs.com/)

and some IDE, such as [Visual Sudio Code](https://code.visualstudio.com/) or some other tool or editor.

- [GIT](https://git-scm.com/) is used for version control.