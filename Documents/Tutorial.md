# Angular
## What is Angular?
*Angular* is a Web framework based on standard Web technologies like

- *HTML*
- *CSS*
- *JavaScript/Typescript*

It is a **component** framework. A component is code, which is encapsulated in an *HTML*-like element and can be added to an *HTML* page.
That allows us to build dynamic Web pages and Web applications.

*Angular* addresses modern concepts, such as

- Dependency Injection
- Data Binding
- Reactive Programming

## *Angular CLI*
The *Angular Commandline Interface* (= *Angular CLI*) is a tool, which helps us create new projects, modules, classes, components etc. It can be installed with *Node*'s package manager *npm*.

### Installation
Open a terminal window and run

```
npm install -g @angular/cli
```

### Using the *Angular CLI*
The *Angular CLI* is a program, whose name is *ng*. If you run

```
ng --help
```

then you will get a list of the available *ng* commands.

You can check the version of the *Angular CLI* with

```
ng version
```

### Setting up a New Project
You can easily set up a new *Angular* project with

```
ng new <project-name>
```

**Example**

If you type

```
ng new demo
```

then the *Angular CLI* will create a folder `demo`, in which the basic structure of an *Angular* project will be generated.

You will be asked, if you want to add navigation to your project (which we will decline for the moment being) and what kind of styling you wish to use *CSS*, *SCSS*, *Sass* or *Less*. *CSS* will do.

The structure of the project is like this

![Project Structure](./Images/project-structure.png)

#### Version Control
*Angular CLI* automatically adds version control to your project. The `.git` folder contains the different versions of your project. The `.gitignore` file defines, which files and folders of your project should be ignored and not versioned.

#### Node Modules
All libraries/modules needed and used in your project are located in the `node_modules` folder. It is excluded from version control. If you remove it, you can easily recreate it by executing

```
npm install
```

in your project folder.

#### Configuration
Most *JSON* files in the root folder of your project are configuration files.

The `tsconfig.*.json` files are used for the configuration of the *Typescript* compiler.

`angular.json` is the *Angular*-specific configuration file.

Finally `package.json` is the general configuration file for *Node* projects.

#### Application
An *Angular* module (`app.module.ts`) and a root component (`app.component.ts`) for your application are created in the `src/app` folder.

The starting point of your Web application is `index.html`.

## *Angular* Application
### Building the Application
If you install additional *Node* modules etc., then you might need to rebuild the project.

Use

```
npm install
```

### Running the Application
During development you can run the application by calling

```
ng serve --open
```

This will

- compile the application,
- start a development server and
- open a browser window in which the application appears.

You can also run

```
ng serve
```

This will only start the development server, but **not** open a browser window. You can open a browser yourself and enter

```
localhost:4200
```

in the address bar of the browser.

Your application should look something like this

![New Angular Application](./Images/new-angular-application.png)
